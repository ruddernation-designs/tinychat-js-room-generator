# Tinychat-JS-Room-Generator
Simple Tinychat random room generator in JavaScript.
This is using my instant room domain for the output but you can use the script from my [instant room](https://github.com/Ruddernation-Designs/Instant-Tinychat).

[Demo](https://www.ruddernation.com/form/room/)
